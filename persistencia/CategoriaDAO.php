<?php
class CategoriaDAO{
    private $idCategoria;
    private $nombre;
    
    public function TipoProductoDAO($idCategoria="", $nombre=""){
        $this -> idCategoria = $idCategoria;
        $this -> nombre = $nombre;
    }
    
    public function consultar(){
        return "select nombre
                from categoria
                where idCategoria = " . $this -> idCategoria;
    }
    
    public function consultarTodos(){
        return "select idCategoria, nombre
                from categoria
                order by nombre asc";
    }
    
    
    
}