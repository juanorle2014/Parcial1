<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $precio;
    private $Categoria_idCategoria;
      
    
    public function ProductoDAO($idProducto="", $nombre="", $precio="", $Categoria_idCategoria=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> Categoria_idCategoria = $Categoria_idCategoria;
        
    }
    
    public function crear(){
        return "insert into producto (nombre, precio, Categoria_idCategoria)
                values (
                '" . $this -> nombre . "',
                '" . $this -> precio . "',
                '" . $this -> Categoria_idCategoria . "'
                )";
    }
    
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        return "select idProducto, nombre, precio,Categoria_idCategoria
                from producto " . 
                (($atributo != "" && $direccion != "")?"order by " . $atributo . " " . $direccion:"") . 
                " limit " . (($pag-1)*$filas) . ", " . $filas; 
    }
    
    public function consultarTotalFilas(){
        return "select count(idProducto) 
                from producto";
    }
    
}