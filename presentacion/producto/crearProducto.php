
<div class="container">
	<div class="row mt-3">
		<div class="col-sm-0 col-md-3"></div>
		<div class="col-sm-12 col-md-6">
			<div class="card">
				<h5 class="card-header "style="background:#BB8FCE ">Crear Producto</h5>
				<div class="card-body">
					
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>" method="post">
						<div class="mb-3">
							<label class="form-label">Nombre</label>
							<input type="text" class="form-control" name="nombre" required="required">
						</div>
						<div class="mb-3">
							<label class="form-label">Precio</label>
							<input type="number" class="form-control" name="precio" required="required">
						</div>
						<div class="mb-3">
							<label class="form-label">Cantidad</label>
							<input type="number" class="form-control" name="cantidad" required="required">
						</div>
<!-- 						<div class="mb-3"> -->
<!-- 							<label class="form-label">Imagen</label> -->
<!-- 							<input type="file" class="form-control" name="imagen" > -->
<!-- 						</div> -->
						<div class="mb-3">
							<label class="form-label">Marca</label>
							<select class="form-select" name="marca">
											
							</select>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
