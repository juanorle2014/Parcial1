<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";
class producto{
    private $idProducto;
    private $nombre;
    private $precio;
    private $Categoria_idCategoria;
    private $conexion;
    private $productoDAO;
    
    /**
     * @return string
     */
    public function getCategoria_idCategoria()
    {
        return $this->Categoria_idCategoria;
    }

    /**
     * @return Conexion
     */

    /**
     * @return string
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    
    /**
     * @return string
     */
   

    public function Producto($idProducto="", $nombre="", $precio="", $Categoria_idCategoria=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> Categoria_idCategoria = $Categoria_idCategoria;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($idProducto, $nombre, $precio, $Categoria_idCategoria);
    }
    
    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> crear());
        $this -> conexion -> cerrar();
    }
        
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        $this -> conexion -> abrir(); 
        echo $this -> productoDAO -> consultarTodos($atributo, $direccion, $filas, $pag);
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos($atributo, $direccion, $filas, $pag));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $Categoria_idCategoria = new categoria($resultado[3]);
            $Categoria_idCategoria -> consultar();
            array_push($productos, new Producto($resultado[0], $resultado[1], $resultado[2], $Categoria_idCategoria));            
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarTotalFilas(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTotalFilas());
        return $this -> conexion -> extraer()[0];
    }
    

}