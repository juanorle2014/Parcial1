<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CategoriaDAO.php";
class categoria{
    private $idCategoria;
    private $nombre;
    private $conexion;
    private $CategoriaDAO;
        
    /**
     * @return string
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function categoria($idCategoria="", $nombre=""){
        $this -> idCategoria = $idCategoria;
        $this -> nombre = $nombre;
        $this -> conexion = new Conexion();
        $this -> CategoriaDAO = new CategoriaDAO($idCategoria, $nombre);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CategoriaDAO -> consultarTodos());
        $tiposProducto = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($tiposProducto, new categoria($resultado[0], $resultado[1]));            
        }
        $this -> conexion -> cerrar();
        return $tiposProducto;
    }
    
}
